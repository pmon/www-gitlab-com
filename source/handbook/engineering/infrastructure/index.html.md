---
layout: markdown_page
title: "Infrastructure"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common Links

| **Workflow** | [**How may we be of service?**](production/#workflow--how-we-work) | **GitLab.com Status** | [**`STATUS`**](https://status.gitlab.com/)
| **Issue Trackers** | [**Infrastructure**](https://gitlab.com/gitlab-com/infrastructure/issues/): [Milestones](https://gitlab.com/gitlab-com/infrastructure/milestones), [OnCall](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=oncall) | [**Production**](https://gitlab.com/gitlab-com/production/issues/): [Incidents](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=incident), [Changes](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=change), [Deltas](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=delta)  | [**Delivery**](https://gitlab.com/gitlab-org/release/framework)
| **Slack Channels** | [#sre-lounge](https://gitlab.slack.com/archives/sre-lounge), [#database](https://gitlab.slack.com/archives/database) | [#alerts](https://gitlab.slack.com/archives/alerts), [#production](https://gitlab.slack.com/archives/production) | [#g_delivery](https://gitlab.slack.com/archives/g_delivery)
| **Operations** | [Runbooks](https://gitlab.com/gitlab-com/runbooks) (please contribute!) | **On-call**: [Handover Document](https://docs.google.com/document/d/1IrTi06fUMgxqDCDRD4-e7SJNPvxhFML22jf-3pdz_TI), [Reports](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=oncall%20report) |

## Other Pages
{:.no_toc}

| **GitLab.com** | [Architecture](/handbook/engineering/infrastructure/production-architecture/) | [Environments](/handbook/engineering/infrastructure/environments/) | [Monitoring](/handbook/engineering/monitoring/) | [Performance](/handbook/engineering/performance/) |
| **Production** | [SRE Onboarding](/handbook/engineering/infrastructure/sre-onboarding/) | [Readiness Guide](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md) | [Database Reliability](/handbook/engineering/infrastructure/database/) | [On-call Handover](/handbook/engineering/infrastructure/on-call-handover/) |

- [Production team handbook](/handbook/engineering/infrastructure/production/)
- [GitLab.com and GitLab Hosted data breach notification policy](/security/#data-breach-notification-policy)

## Mission

The **Infrastructure Department** is the primary responsible party for the **availability**,
**reliability**, **performance**, and **scalability** of all user-facing services (most
notably **GitLab.com**, the largest production GitLab Installation on the planet). Other 
departments and teams contribute greatly to these attributes of our service as well. In these
cases it is the responsibility of the Infrastructure Department to close the feedback loop
with monitoring and metrics to drive accountability.

## Vision

We are a blend of operations gearheads and software crafters that apply sound enginering
principles, operational discipline and mature automation to make GitLab.com ready for
mission-critical customer workloads. We strive for excellence every day by living and
breathing [**GitLab's values**](/handbook/values/) as our guiding operating principles in
every decision we make and every action we take.

## Blueprints, Designs, and OKRs

[**Blueprints**](blueprint/) are intended to scope flesh out our initial thinking about
specific problems and issues we are facing (topical) and outline overall Infrastructure
priorities and focus for a given quarter (quarterly). Blueprints are sketches whose purpose
is to foster and frame discussion around Infrastructure topics, most of which will yield
designs and OKRs, which qualify and quantify objectives and key results.

[**Design**](design/) plays a significant role in how we produce technical solutions to meet
the challenges we face in making GitLab.com ready for mission-critical workloads.

## Teams

The Infrastructure Department is comprised of three teams:

* [**Site Availability**](team/sae/), which operates on the _here_ and _now_ and is focused on uptime as its driving force.
* [**Site Reliability**](team/sre/), which operates on the _soon_ time horizon and is focused on efficiency, and of course, reliability.
* [**Delivery**](team/delivery/), which focuses on GitLab's delivery of software releases to GitLab.com and the public at large

For details on the Department's structure, see the [**Infrastructure Teams Handbook section**](team/).

### SRE Stable Counterparts

Every SRE is aligned with an engineering team. Each SRE can help the teams at each stage of the process. Planning, discovery, implementation, and further iteration. The area an SRE is responsible for is part of their title, e.g. "SRE, Plan, Monitor." You can see which area of the product each SRE is aligned with in the [team org chart](/company/team/org-chart/).

Multiple SREs are aligned with areas of the product. This area will be listed on the [team page](/company/team/) under their title as an expertise, e.g. "Plan expert."  This way there is a team of SREs available to provide help in the case that another is out of the office or busy with another incident or team.

|Team|SRE|SRE|SRE|
|----|---|---|---|
|Monitor|Ahmad Sherif|Amarbayar Amarsanaa|John Skarbek|
|Secure|John Skarbek|Craig Barrett|Alejandro Rodriguez|
|Configure|Craig Barrett|John Northrup|Devin Sylva|
|Verify (CI) / Release (CD)|John Northrup|Devin Sylva|Alex Hanselka|
|Serverless|Andrew Newdigate|John Jarvis|John Northrup|
|Distribution and Packaging|John Jarvis|Alex Hanselka|Craig Barrett|
|Create|Alex Hanselka|John Jarvis|Amarbayar Amarsanaa|
|Plan|Craig Barrett|Amarbayar Amarsanaa|John Skarbek|
|Manage|Devin Sylva|Ahmad Sherif|John Northrup|
|Gitaly|Andrew Newdigate|Alejandro Rodriguez|Ahmad Sherif|
|Gitter|Andrew Newdigate|Ahmad Sherif|Alejandro Rodriguez|
|Geo|Alex Hanselka|John Skarbek|John Jarvis|

