---
layout: markdown_page
title: "Engineering Manager, Distribution and Delivery"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Purpose

Inspired by the [CEO page](https://about.gitlab.com/handbook/ceo/) and [Director of Dev Backend readme](/handbook/engineering/dev-backend/director/),
this page is a readme for the current Engineering Manager of Distribution and Delivery teams,
[Marin Jankovski](/company/team/#maxlazio).

## Team(s)

I am currently the primary engineering manager for two teams, [Distribution](/handbook/engineering/dev-backend/distribution)
and [Delivery](/handbook/engineering/infrastructure/delivery).
The history behind why I am leading these two teams is a bit long and digs into GitLab and my career development path at GitLab
so I'll skip detailing it for now.

The two teams have a clear mission and vision, and if you read them side by side you'll notice some
similarities.
Namely, Distribution team is focused on shipping GitLab as a whole to our customers and users
and Delivery team focuses on enabling engineering teams within GitLab to get their work
ready for shipping *and* running on GitLab.com.

In other words, Delivery team primary focus is on tasks prior to releasing GitLab to public
and Distribution on tasks for customer/user consumption.

## Time allocation

Between enabling every team member of both teams to do their jobs the best they can, engineering tasks
and hiring, my work day is a stream of context switching.

The work week can however be roughly divided as:

  * 1 day a week for all 1-1s with all my direct reports
  * 2 days a week for Distribution team related tasks
  * 2 days a week for Delivery team related tasks

### 1-1s

I hold weekly 1-1s with all my direct reports. Most common call duration is 30 minutes
but this vastly depends on the individual report. In all cases, I find it acceptable
when the meetings are both shorter and longer but I dislike skipping these calls.

I've found that important information gets lost when 1-1s are skipped because
the smallest, most insignificant looking "btw" often can save a lot of time and
effort down the line.
I don't often quote other people so I won't do that now too, but I'll just say that
[High Output Management - Andrew Grove] is very often right in my opinion.

My 1-1s do have some structure, but only as a guideline in case people are not
inspired (which can often be the case when you have the same type of call every week).

The structure consists of:

* Every meeting has the Google Doc accessible to the direct report and myself only
* The doc has a suggested topics template at the top that the report can use if they want to
* The doc is filled by the report 1 working day before the meeting
* The doc can be used as a place to write down all the thoughts, positive and negative
* In case of an item that requires attention prior to the scheduled call, the report can mention
me or raise it up in direct message in Slack

## Distribution team

My tasks in the Distribution team are related to hiring, working with the Distribution
Product Manager on planning and scheduling features and working together with the
team on optimising team processes.

Day to day technical decisions are made by each team member. My technical tasks are
limited to providing advice, providing higher level guidance and help with making
a decision.

Periodically I will help with Merge Request review in cases where change impact is large.

## Delivery team

Delivery team is inward facing so majority of my work is looking into
engineering processes and driving a change that helps us deliver faster.
Large chunk of this is working on direction blueprints that not only allows
engineers to work more efficiently, but also changes in the way we deploy our
code into GitLab.com environments.

## My Availability

* My calendar is generally blocked during the early morning/evening. I usually do not
accept meetings during this period
* I have a work only laptop which contains only one non-work related account: music streaming application.
If I am responding during my off hours, that usually means that I am traveling or I have a pretty
big behaviour regression
* I do not have Slack or work email setup on my phone so I will not respond to
pings unless I am at my work laptop

## Communication

* I have a very direct communication style, sometimes mistaken with being upset at people.
If you encounter that, please ask whether that is the case.
* English is not my native language which can affect how I express myself.
I tend to mix words like `frustrated` and `upset` and sometimes oversimplify sentences.
I will also say `I don't understand` often when I am having trouble translating what is being said. Please ask for clarification if you encounter this.
* I have been at GitLab since the start and have a strong sense of ownership.
Due to this, I sometimes tend to be overprotective if I think that something won't benefit
GitLab. If you explain how an item will help GitLab in general and not individual/team
I will look into ways of helping if I agree.
* I do not like inefficiency and will look for any way to improve that. This is also
the case in group conversations where bike-shedding is making me uncomfortable and
I tend to speak up very directly regardless of the list of participants. If this
makes you feel uncomfortable, please schedule a call with me and provide me feedback.
* I am not an optimist person by nature so I tend to see everything as glass half empty.
There is always room for improvement and I always stress the improvement part. In recent
times I've been learning on how to provide feedback that is positive in nature, and I've
asked people to help me with that by asking for that type of feedback specifically.
If I am not able to provide it on the spot, give me time to think as that takes an extra
effort from my side.

## Trust

My default position is to trust everyone, be it work related or not.
To some people I come off as too open and free as I expect people to say when they
don't want to share some information or are not in agreement.

At work, I will trust that intentions are good by default. I will often provide
feedback if I see something that I find is not beneficial to the company whether
that is my job or not.
I will provide direct feedback to individuals of all ranks if I observe this type
of behaviour.

If I feel that I am repeating my feedback or that my feedback is being
misused without raising this directly with me, I tend to react by withdrawing the
trust completely. From then on, earning back my trust is a lot of work.

For this reason I ask people working with me to always provide me direct feedback
in time, and to react to my feedback timely without holding back.

[High Output Management - Andrew Grove]: https://www.amazon.com/High-Output-Management-Andrew-Grove/dp/0679762884
