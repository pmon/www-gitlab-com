---
layout: markdown_page
title: "Signing legal documents"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Only C-level executives can sign legal documents, with the exception of NDAs covering a physical visit of another organization.
When working with legal agreements with vendors, consultants, and so forth, bear in mind the [signature authorization matrix](/handbook/finance/authorization-matrix/).
If you need to obtain approval for a vendor contract, please create a confidential issue in the finance issue tracker using our [Contract Approval Workflow](https://about.gitlab.com/handbook/finance/procure-to-pay/).

For all other documents that need to be signed, filled out, sent, or retrieved electronically, please do your best to fill out the form using the handbook and [wiki](/gitlab-com/finance/wikis/company-information) then e-mail it to `legal@` with the following information:

1. Names and email addresses of those who need to sign the document.
1. Any contractual information that needs to be included in the document.
1. Deadline (or preferred timeline) by which you need the document prepared (i.e. staged in [HelloSign](https://www.hellosign.com) for relevant signatures)
1. Names and email addresses of those who need to be cc-ed on the signed document.

The process that Legal will follow is:

1. Review the document and prepare as requested.
1. Have the requestor check the prepared document, AND obtain approval from the CFO or CEO (such approval may be explicit in the email thread that was sent to `legal@`, in which case a second approval is not needed unless there have been significant edits to the document).
1. Requestor shall stage the document for signing in HelloSign and cc (at minimum) `legal@`.
1. Once signed: 
a. For customer contracts, the Requestor needs to attach the document to the applicable Contracts Object in Salesforce, and fill out all applicable fields in the Contracts Object. 
b. For vendor agreements, Requestor will [file the document in ContractWorks](https://about.gitlab.com/handbook/legal/vendor-contract-filing-process/index.html).
